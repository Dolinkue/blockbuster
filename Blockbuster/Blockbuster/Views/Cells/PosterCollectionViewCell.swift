//
//  PosterCollectionViewCell.swift
//  Blockbuster
//
//  Created by Bryan A Bolivar M on 24/10/22.
//

import UIKit

class PosterCollectionViewCell: UICollectionViewCell {
    @IBOutlet weak var movieImageView: UIImageView!

    var movie: Movie? {
        didSet {
            guard let movie = movie else {
                return
            }
            
            if let url = movie.imageURLRepresentation() {
                movieImageView.kf.setImage(with: url)
            } else {
               // movieImageView.backgroundColor = movie.genre.color()
            }
        }
    }
}
