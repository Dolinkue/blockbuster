//
//  MoviesViewController.swift
//  Blockbuster
//
//  Created by Bryan A Bolivar M on 20/10/22.
//

import UIKit
import Alamofire

struct MovieListResponse: Codable {
    let page: Int
    let results: [Movie]
}

class MoviesViewController: UIViewController {
    @IBOutlet weak var searchTextField: UITextField!
    @IBOutlet weak var tableView: UITableView!
    let refreshControl = UIRefreshControl()
    var movies: [Movie] = []
    var selectedMovie: Movie?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        tableView.dataSource = self
        tableView.delegate = self
        fetchMovies()
        tableView.addSubview(refreshControl)
        refreshControl.addTarget(self, action: #selector(fetchMovies), for: .valueChanged)
        
        tableView.keyboardDismissMode = .onDrag
        searchTextField.delegate = self
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.title = "Movies List"
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)       
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "MovieDetailsSegue" {
            let vc = segue.destination as! MovieDetailsViewController
            vc.movie = selectedMovie
        }
    }
    
    @objc func fetchMovies() {
        refreshControl.endRefreshing()
        searchTextField.text = ""
        searchTextField.endEditing(true)
        let urlParams = [
            "api_key":"627e3f67dac2c73e662093fe66b67ce7",
        ]
        
        // Fetch Request
        AF.request("https://api.themoviedb.org/3/movie/popular", method: .get, parameters: urlParams)
            .validate(statusCode: 200..<300)
            .responseJSON { response in
                debugPrint(response)
                do {
                    guard let data = response.data else {
                        print("Data parsing failure, not found")
                        return
                    }
                    let movies = try JSONDecoder().decode(MovieListResponse.self, from: data)
                    self.movies = movies.results
                    DispatchQueue.main.async {
                        self.tableView.reloadData()
                    }
                } catch {
                    print(error.localizedDescription)
                }
            }
    }
    
    @IBAction func searchMovie(_ sender: Any) {
        guard
            let movieName = searchTextField.text,
            !movieName.isEmpty
        else { return }
        
        let urlParams = [
            "api_key":"627e3f67dac2c73e662093fe66b67ce7",
            "query": movieName
        ]

        // Fetch Request
        AF.request("https://api.themoviedb.org/3/search/movie", method: .get, parameters: urlParams)
            .validate(statusCode: 200..<300)
            .responseJSON { response in
                debugPrint(response)
                do {
                    guard let data = response.data else {
                        print("Data parsing failure, not found")
                        return
                    }
                    let movies = try JSONDecoder().decode(MovieListResponse.self, from: data)
                    self.movies = movies.results
                    DispatchQueue.main.async {
                        self.tableView.reloadData()
                    }
                } catch {
                    print(error.localizedDescription)
                }
            }
    }
    
    @IBAction func textChanged(_ sender: Any) {
        print("textChanged")
        searchMovie(self)
    }

}

extension MoviesViewController: UITableViewDataSource {
    
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard
            let cell = tableView.dequeueReusableCell(withIdentifier: "MovieCellIdentifier", for: indexPath) as? MovieTableViewCell
        else {
            return UITableViewCell()
        }
        cell.movie = movies[indexPath.row]
        return cell
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return movies.count
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 100
    }
}

extension MoviesViewController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        selectedMovie = movies[indexPath.row]
        performSegue(withIdentifier: "MovieDetailsSegue", sender: self)
    }
}

extension MoviesViewController: UITextFieldDelegate {
    func textFieldDidEndEditing(_ textField: UITextField) {
        print("textFieldDidEndEditing")

    }
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        print("textFieldDidBeginEditing")

    }
}


/*
 bloquear busqueda hasta que hayan pasado 3 segundos despues de la ultima edicion
 TIP: DispatchAfter + State
 */
