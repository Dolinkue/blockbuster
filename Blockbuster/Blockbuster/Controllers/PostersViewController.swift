//
//  PostersViewController.swift
//  Blockbuster
//
//  Created by Bryan A Bolivar M on 24/10/22.
//

import UIKit
import Alamofire

class PostersViewController: UIViewController {

    var movies: [Movie] = []
    var selectedMovie: Movie?
    
    
    @IBOutlet weak var collectionView: UICollectionView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        fetchMovies()
        title = "Posters"
        collectionView.dataSource = self
        collectionView.delegate = self
        collectionView.collectionViewLayout = UICollectionViewFlowLayout()
        
    }
    
    @objc func fetchMovies() {
        let urlParams = [
            "api_key":"627e3f67dac2c73e662093fe66b67ce7",
        ]
        
        // Fetch Request
        AF.request("https://api.themoviedb.org/3/movie/popular", method: .get, parameters: urlParams)
            .validate(statusCode: 200..<300)
            .responseJSON { response in
                debugPrint(response)
                do {
                    guard let data = response.data else {
                        print("Data parsing failure, not found")
                        return
                    }
                    let movies = try JSONDecoder().decode(MovieListResponse.self, from: data)
                    self.movies = movies.results
                    DispatchQueue.main.async {
                        self.collectionView.reloadData()
                    }
                } catch {
                    print(error.localizedDescription)
                }
            }
    }

}

extension PostersViewController: UICollectionViewDataSource, UICollectionViewDelegateFlowLayout  {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return movies.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "PosterCellIdentifier", for: indexPath) as! PosterCollectionViewCell
        cell.movie = movies[indexPath.row]
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView,
                        layout collectionViewLayout: UICollectionViewLayout,
                        sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        return CGSize(width: collectionView.frame.size.width/3.5, height: collectionView.frame.size.height/4)

    }


    
}

extension PostersViewController: UICollectionViewDelegate {
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        selectedMovie = movies[indexPath.row]
        performSegue(withIdentifier: "MovieDetailsSegue", sender: self)
    }
}



/*
 Mostrar cells de 3 en 3 respetando las proporciones del telefono
 */
