//
//  ViewController.swift
//  Blockbuster
//
//  Created by Bryan A Bolivar M on 19/10/22.
//

import UIKit

class ViewController: UIViewController {

    @IBOutlet weak var emailTextField: UITextField!
    @IBOutlet weak var passwordTextField: UITextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
    }
    
    @IBAction func presentDetails(_ sender: Any) {
        performSegue(withIdentifier: "MoviesSegue", sender: self)
    }
    
    @IBAction func performSignIn(_ sender: Any) {
        if (emailTextField.text == defaultUser.email) && (passwordTextField.text == defaultUser.password) {
            print("Welcome back", defaultUser.name)
        } else {
            print("Login Failed, try again")
        }
    }
    
}

