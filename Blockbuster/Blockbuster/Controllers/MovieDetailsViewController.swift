//
//  MovieDetailsViewController.swift
//  Blockbuster
//
//  Created by Bryan A Bolivar M on 21/10/22.
//

import UIKit
import Alamofire

class MovieDetailsViewController: UIViewController {
    
    @IBOutlet weak var movieImageView: UIImageView!
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var genreLabel: UILabel!
    
    var movie: Movie?

    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        setupViewComponents()
    }
    
    func setupViewComponents() {
        guard let movie = movie else {
            return
        }
        
        nameLabel.text = movie.name
        //genreLabel.text = movie.genre.rawValue
        
        if let url = movie.imageURLRepresentation() {
            movieImageView.kf.setImage(with: url)
        } else {
           // movieImageView.backgroundColor = movie.genre.color()
        }
    }
    


}
