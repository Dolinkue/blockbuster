//
//  User.swift
//  Blockbuster
//
//  Created by Bryan A Bolivar M on 19/10/22.
//

import Foundation

struct User {
    let name: String
    let email: String
    let age: Int
    let password: String
}

let defaultUser = User(name: "Bryan", email: "bolivarbryan@gmail.com", age: 33, password: "123456")
