//
//  Movie.swift
//  Blockbuster
//
//  Created by Bryan A Bolivar M on 20/10/22.
//

import Foundation
import UIKit

enum MovieGenre: String, Codable {
    case horror
    case comedy
    case fantasy
    case action
    
    func canWatchMovie(age: Int) -> Bool {
        switch self {
        case .horror:
            return age >= 18
        case .action:
            return age > 14
        case .comedy:
            return true
        case .fantasy:
            return age > 4
        }
    }
    
    func color() -> UIColor {
        switch self {
        case .horror:
            return .red
        case .fantasy:
            return .magenta
        case .action:
            return .orange
        case .comedy:
            return .green
        }
    }
}

struct Movie: Codable {
    
    /// The movie Title
    let name: String
    
    /// The movie overview
    let description: String
    
    //var genre: MovieGenre = .action
    
    /// The poster Path
    let image: String?
    
    enum CodingKeys: String, CodingKey {
        case name = "title"
        case description  = "overview"
        //case genre
        case image = "poster_path"
    }
    
    func movieDetails(age: Int) -> String {
        return ""
    }
    
    func imageURLRepresentation() -> URL? {
        guard let image = image else {
            return URL(string:  "https://media.istockphoto.com/vectors/error-page-or-file-not-found-icon-vector-id924949200?k=20&m=924949200&s=170667a&w=0&h=-g01ME1udkojlHCZeoa1UnMkWZZppdIFHEKk6wMvxrs=")
        }
        return URL(string:  "https://image.tmdb.org/t/p/w500/" + image)
    }
    
}

