import UIKit

enum VehicleType: String {
    case car = "Carro"
    case bicycle = "Bici"
    case motorbike = "Moto"
    case plane = "Avion"
}

let myBike = VehicleType.motorbike

func printVehicle(vehicle: VehicleType) -> String {
    switch vehicle {
    case .car:
        return "Es un Carro"
    case .bicycle:
        return "Es una bici"
    case .motorbike:
        return "es una moto"
    case .plane:
        return "es un avion"
    }
}

let number9 = 9
let isFalse = false
let isTrue = true

switch 9 {
case (0...8):
    print("is 5")
case (10...1000):
    print("no deberia caer")
case 9:
    print("9")
default:
    print("9")
}


/*switch (isTrue, isFalse) {
 case (true, false):
 case (false, false):
 case (true, true):
 case (false, true):
 }
 */


//---
enum MovieGenre {
    case horror
    case comedy
    case fantasy
    case action
    
    func canWatchMovie(age: Int) -> Bool {
        switch self {
        case .horror:
            return age >= 18
        case .action:
            return age > 14
        case .comedy:
            return true
        case .fantasy:
            return age > 4
        }
    }
}

struct Movie {
    let id: UUID
    let name: String
    let description: String
    let genre: MovieGenre
    
    func movieDetails(age: Int) -> String {
        if genre.canWatchMovie(age: age) {
            return description
        } else {
            return "you are allowed to see this movie, pick another one"
        }
    }
}

struct User {
    let id: UUID
    let name: String
    let age: Int
}

let saw3 = Movie(id: UUID(), name: "Saw 3", description: "Una peli no apta para todos", genre: .horror)
let lordOfTheRings = Movie(id: UUID(), name: "LOTR", description: "Pelicula de peter jackson", genre: .fantasy)
let whiteChicks = Movie(id: UUID(), name: "White Chicks", description: "Gemelos policiacos metidos en temas de peluca", genre: .comedy)
let transporter = Movie(id: UUID(), name: "The Transporter", description: "un hombre que puede voltear un carro y no perder un pelo", genre: .comedy)

let movieCollection: [Movie] = [saw3, lordOfTheRings, whiteChicks, transporter]

func whatMoviesCanUserSee(collection: [Movie], user: User) -> [String] {
    return collection
        .filter { movie in
            return movie.genre.canWatchMovie(age: user.age)
        }.map { movie in
            movie.name
        }
}


let suzy = User(id: UUID(), name: "Suzy", age: 14)

saw3.movieDetails(age: suzy.age)

print(whatMoviesCanUserSee(collection: movieCollection, user: suzy))

/*
 CompactMap
 FlatMap
 Reduce
*/
